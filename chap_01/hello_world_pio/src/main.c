#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
 
void hello_task(void *pvParameter)
{
    printf("Welcome to world of microcontroller\n");
    vTaskDelay(1000 / portTICK_RATE_MS);
    
    fflush(stdout);
    esp_restart();
}
 
void app_main()
{
    // nvs_flash_init();
    xTaskCreate(&hello_task, "hello_task", 2048, NULL, 5, NULL);
}
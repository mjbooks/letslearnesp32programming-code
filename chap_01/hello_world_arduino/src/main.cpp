
#include <Arduino.h>

#define LED 2

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);  // Baud Rate = monitor_speed = 115200
  pinMode(LED, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("Welcome to world of microcontroller");
  delay(1000);
  Serial.println("!!!!");
}

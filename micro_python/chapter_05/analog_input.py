from machine import Pin
from time import sleep
from machine import ADC

pin_num = 34
potValue = 0
pot = Pin(pin_num, Pin.IN)

adc = ADC(pot)
while True:
    print(adc.read_u16())
    print(adc.read_uv())
    print(20*"_")
    sleep(0.22)


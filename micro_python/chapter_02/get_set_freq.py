import machine

freq = machine.freq()
print("Initial freq:", freq)  # get the current frequency of the CPU
machine.freq(240000000)  # set the CPU frequency to 240 MHz
new_freq = machine.freq()
print("Updated freq:", new_freq)  # get the current frequency of the CPU
print(machine.freq())  # get the current frequency of the CPU
machine.freq(freq)  # reset the CPU frequency to original MHz
# machine.reset()

import ujson

try:
    import urequest as request
except:
    import upip
    print("Lets install urequest")

    upip.install("micropython-urequests")

    import urequests as requests

_URL = ""

def set_realdb_URL(url):
    global _URL
    _URL = url
    

def get_auth_url(name):
    _auth_url = "https://www.googleapis.com/identitytoolkit/v3/relyingparty"
    urls = {
        "sign_in" : f"{_auth_url}/verifyPassword",
        "sign_up" : f"{_auth_url}/accounts:signUp",
        "sign_out" : f"{_auth_url}/accounts:signUp",
        "verify": f"{_auth_url}/verifyPassword"
        }
    return urls[name] + "?key={api_key}"

def add_user(user_email, password, api_key):
    _url = get_auth_url("sign_up").format(api_key=api_key)
    post_data = ujson.dumps({"email":user_email,"password":password,"returnSecureToken":True})
    res = requests.post(_url, headers = {'content-type': 'application/json'}, data = post_data)
    header_data = ''
    print(dir(res))
    if res.status_code == 200 :
        res_data = res.json()
        print(res_data)
        header_data = { "content-type": 'application/json; charset=utf-8'}
        for header in ("idToken", "email", "refreshToken", "expiresIn", "localId"):
            header_data[header] = str(res_data[header])
    else:
        return False, res.json()
    return True, header_data

def validate_user(user_email, password, api_key):
    _url = get_auth_url("sign_in").format(api_key=api_key)
    print(f"url: {_url}")
    post_data = ujson.dumps({"email":user_email,"password":password,"returnSecureToken":True})
    print(help(requests.post))
    res = requests.post(_url, headers = {'content-type': 'application/json'}, data = post_data)
    header_data = ''
    print(dir(res))
    print(res.text)
    if res.status_code == 200 :
        res_data = res.json()
        print(res_data)
        header_data = { "content-type": 'application/json; charset=utf-8'}
        for header in ("idToken", "email", "refreshToken", "expiresIn", "localId"):
            header_data[header] = str(res_data[header])
    else:
        return False, res.json()
    return True, header_data

def populate_data(header, rel_url, data):
    id_token = header['idToken']
    _url = f"{_URL}/{rel_url}/.json?auth={id_token}"
    print(f"{_url=}")
    post_data = ujson.dumps(data)
    header['content-type'] = 'application/json'
    res = requests.post(_url, headers = header, data = post_data)
    header_data = ''
    if res.status_code == 200 :
        res_data = res.json()
    else:
        return False, res.json()
    return True, header_data


def patch_data(header, rel_url, data):
    id_token = header['idToken']
    _url = f"{_URL}/{rel_url}/.json?auth={id_token}"
    print(f"{_url=}")
    post_data = ujson.dumps(data)
    header['content-type'] = 'application/json'
    res = requests.patch(_url, headers = header, data = post_data)
    header_data = ''
    if res.status_code == 200 :
        res_data = res.json()
    else:
        return False, res.json()
    return True, header_data

